#include "MinPriorityQueue.h"
#include "SortingAlgorithms.h"
#include "SortingAlgorithmsUtil.h"

void AscendingHeapSort(int* arr, int size, int arity) {
	MinPriorityQueue* minpq = CreateMinPriortyQueue(size, arity);
	int i;

	/* YOUR CODE STARTS HERE */
	for (i = 0; i < size; i++) {
		InsertToMinPQ(minpq, arr[i]);
	}

	for (i = 0; i < size; i++) {
		arr[i] = ExtractMinPQMinimum(minpq);
	}
	/* YOUR CODE ENDS HERE */

	FreeMinPQ(minpq);
}

void DescendingHeapSort(int* arr, int size, int arity) {
	MinPriorityQueue* minpq = CreateMinPriortyQueue(size, arity);
	int i;

	/* YOUR CODE STARTS HERE */
	for (i = 0; i < size; i++) {
		InsertToMinPQ(minpq, arr[i]);
	}

	for (i = size - 1; i >= 0; i--) {
		arr[i] = ExtractMinPQMinimum(minpq);
	}
	/* YOUR CODE ENDS HERE */

	FreeMinPQ(minpq);
}

// Works on the sub-array given by 'p' (as lower bound) and 'r' (as higher bound).
// Returns the index in the given 'arr' into which the current pivot (arr[r]) was moved.
// All elements before the returned indices should be smaller than arr[r], while all elements
// after it should be equal or larger from it.
// RUNTIME: Runs in O(p-r).
int Partition(int* arr, int p, int r) {
	int pivot = arr[r];
	int q = p;
	int j;
	/* YOUR CODE STARTS HERE */
	for (j = r - 1; q<j; j--)
	{
		if (arr[j] < pivot)
		{
			if (j != q) {
				SwapIndices(arr, j, q);
				j++;
				q++;
			}
		}
		else {
			arr[r] = arr[j];
			r--;
		}
	}
	arr[q] = pivot;
	/* YOUR CODE ENDS HERE */

	return q;
}

// Works on the sub-array given by 'p' (as lower bound) and 'r' (as higher bound).
// Reorganizes the given 'arr' into 4 subsequent sub-array as follows, using arr[r] as pivot:
// [(elements smaller than pivot)(elements equal to pivot)(pivot)(elements larger than pivot)].
// Updates 'qRes' to hold the start of the (elements equal to pivot) sub-array.
// Updates 'tRes' to hold the index of 'pivot'.
// RUNTIME: Runs in O(p-r).
void SmartPartition(int* arr, int size, int p, int r, int *qRes, int *tRes) {
	int pivot = arr[r];
	int q = p, t = p;
	int j;

	/* YOUR CODE STARTS HERE */
	while (t <= r) {
		if (arr[t]<pivot) {
			if (t != q) {
				SwapIndices(arr, t, q);
			}
			t++;
			q++;
		}
		else {
			if (arr[t]>pivot) {
				if (t != r) {
					SwapIndices(arr, r, t);
				}
				r--;
			}
			else {
				if (arr[t] == pivot)
					t++;
			}
		}
	}
	t = r;
	arr[q] = pivot;
	/* YOUR CODE ENDS HERE */

	(*qRes) = q;
	(*tRes) = t;
}

void StackBasedQuickSort(int* arr, int size) {
	Stack* stack = CreateStack(QuicksortDataStructureLimit(size));
	//Stack* stack = CreateStack(size);
	int p = 0, q = -1, r = size - 1;

	/* YOUR CODE STARTS HERE */
	BOOL toPOP = 1;
	p = 0;
	r = size - 1;
	MaybePushIndices(stack, p, r, size);
	q = 0;
	while (!IsStackEmpty(stack)) {

		if (toPOP) {
			r = Pop(stack);
			p = Pop(stack);
		}
		q = Partition(arr, p, r);
		if ((r - (q + 1)) <= ((q - 1) - p)) {
			if ((q - 1) > p) {
				MaybePushIndices(stack, p, (q - 1), size);
			}
			else { toPOP = 1; }
			
			if ((q + 1) < r) {
				p = q + 1;
				toPOP = 0;
			}
			else { toPOP = 1; }
		}
		else {
			if ((q + 1) < r) {
				MaybePushIndices(stack, (q + 1), r, size);
			}
			else {	toPOP = 1;	}
			if ((q - 1) > p) {
				r = q - 1;
				toPOP = 0;
			}
			else {	toPOP = 1;	}
		}
	}
	/* YOUR CODE ENDS HERE */
	FreeStack(stack);
}


void QueueBasedQuickSort(int* arr, int size) {
	Queue* queue = CreateQueue(2 * size);
	int p = -1, q = -1, r = -1;
	if (DEBUG) {
		printf("QueueBasedQuickSort\n");
	}

	/* YOUR CODE STARTS HERE */
	p = 0;
	r = size - 1;
	//Insert the initial value to the queue
	MaybeEnqueueIndices(queue, p, r, size);
	while (!IsQueueEmpty(queue)) {
		p = Dequeue(queue);
		r = Dequeue(queue);
		if (p < r) {
			q = Partition(arr, p, r);
			MaybeEnqueueIndices(queue, p, (q - 1), size);
			MaybeEnqueueIndices(queue, (q + 1), r, size);
		}
	}
	/* YOUR CODE ENDS HERE */

	FreeQueue(queue);
}

void EqualElementQuickSort(int* arr, int size, int p, int r) {
	int q, t;
	/* YOUR CODE STARTS HERE */
	SmartPartition(arr, size, p, r, &q, &t);
	if (q - 1 - p <= r - t - 1) {
		if (IsValidSubArray(p, q - 1, size) && q - 1>p) {
			EqualElementQuickSort(arr, size, p, q - 1);
		}
		if (IsValidSubArray(t + 1, r, size) && r>t + 1) {
			EqualElementQuickSort(arr, size, t + 1, r);
		}
		return;
	}
	else {
		if (IsValidSubArray(t + 1, r, size) && r>t + 1) {
			EqualElementQuickSort(arr, size, t + 1, r);
		}
		if (IsValidSubArray(p, q - 1, size) && q - 1>p) {
			EqualElementQuickSort(arr, size, p, q - 1);
		}
		return;
	}
	/* YOUR CODE ENDS HERE */

}

// Sorts the IntPair array 'arr' of size 'size' in the following manner, and the following assumptions:
// IntPair elements hold two integers - 'first' and 'second'.
// CountingSort sorts the IntPair elements according the their 'second' values.
// It is assumed that all 'second' values are between [0, limit] inclusive (limit+1 different values).
// RUNTIME: Runs in O(n+k), where n=size and k=limit+1.
void CountingSort(IntPair* arr, int size, int limit) {
	IntPair* c = (IntPair*)calloc(size, sizeof(IntPair));
	int* b = (int*)calloc(limit + 1, sizeof(int));
	int i;

	if (DEBUG) {
		printf("CountingSort\n");
	}

	for (i = 0; i < size; i++) {
		b[arr[i].second]++;
	}
	for (i = 1; i < limit + 1; i++) {
		b[i] += b[i - 1];
	}
	for (i = size - 1; i >= 0; i--) {
		c[b[arr[i].second] - 1] = arr[i];
		b[arr[i].second]--;
	}
	for (i = 0; i < size; i++) {
		arr[i] = c[i];
	}

	free(b);
	free(c);
}

void BaseModuluRadixSort(int* arr, int size, int limit, int base) {
	IntPair *a = (IntPair*)malloc(sizeof(IntPair)*size);
	int i, denominator = 1;

	for (i = 0; i < size; i++) {
		a[i].first = arr[i];
	}

	/* YOUR CODE STARTS HERE */
	//The number of the iteration will be the number of column of the biggest number in the array
	
	for (denominator = 1; limit / denominator > 0; denominator *= 10) {
		for (i = 0; i < size; i++) {
			a[i].second = a[i].first % (denominator * 10);
			a[i].second = a[i].second / denominator;
		}
		CountingSort(a, size, limit);
	}
	/*
	while (denominator <= limit)//while the biggest num is not sorted, do.
	{
		for (i = 0; i < size; i++)
		{
			a[i].second = a[i].first / denominator%base;
		}
		CountingSort(a, size, limit);
		denominator *= base;
	}*/

	/* YOUR CODE ENDS HERE */

	for (i = 0; i < size; i++) {
		arr[i] = a[i].first;
	}
	free(a);
}