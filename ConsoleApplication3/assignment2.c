#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "SortingAlgorithms.h"
#include "Util.h"
#include "Queue.h"

#define SAS(arr) sizeof(arr)/sizeof(arr[0]) // Returns the size of a non empty static array.

#define NON_UNIFORM_PORTION 3

#define MANUAL_MODE 1
#define BUILTIN_MODE 2

#define CONTINUE 1
#define QUIT 2

#define ASCENDING_HEAPSORT 1
#define DESCENDING_HEAPSORT 2
#define STACK_QUICKSORT 3
#define QUEUE_QUICKSORT 4
#define REPEATING_VALUE_QUICKSORT 5
#define RADIXSORT 6

#define HEAPSORT_ALGORITHMS(choice) (choice == ASCENDING_HEAPSORT || choice == DESCENDING_HEAPSORT)

typedef struct benchmarkconfig{
	const int* sizes;
	int length;
	int limit;
	const int* algorithms;
	int num_algos;
	int arity;
	int array_type;
	int iterations;
	int base;
	int** successes;
	double** runtimes;
	double** expected_runtimes;
} BenchmarkConfig;

typedef struct singleconfig {
	int size;
	int limit;
	int algorithm;
	int arity;
	int array_type;
	int iterations;
	int successes;
	double runtime;
} SingleConfig;

static const int benchmark_heapsort_arity_1[] = { 1000, 10000, 30000, 50000, 100000 };
static const int benchmark_heapsort[] = { 1000, 10000, 100000 , 1000000 };
static const int benchmark_quicksort[] = { 1000, 10000, 50000, 100000 , 200000 };
static const int benchmark_radixsort[] = { 1000, 10000, 100000, 1000000, 10000000 };
static const int benchmark_all[] = { 1000, 10000, 100000, 300000 };

static const int heapsort_algorithms[] = { ASCENDING_HEAPSORT, DESCENDING_HEAPSORT };
static const int quicksort_algorithms[] = { STACK_QUICKSORT, QUEUE_QUICKSORT, REPEATING_VALUE_QUICKSORT };
static const int iter_quicksort_algorithms[] = { STACK_QUICKSORT, QUEUE_QUICKSORT};
static const int rec_quicksort_algorithms[] = { REPEATING_VALUE_QUICKSORT };
static const int radixsort_algorithms[] = { RADIXSORT };
static const int all_algorithms[] = { ASCENDING_HEAPSORT, DESCENDING_HEAPSORT, STACK_QUICKSORT,
									  QUEUE_QUICKSORT, REPEATING_VALUE_QUICKSORT, RADIXSORT };

#define NUM_ITERATIONS 10
#define DEFAULT_LIMIT 10000
#define RADIXSORT_LIMIT 10000000

#define NORMALIZE_ARRAY_TYPE(choice) choice-1
#define UNIFORMAL_ARRAY 1
#define REPEATING_VALUE_ARRAY 2

#define RUNTIME_MULTIPLIER 1000000

#define HEAPSORT_CHOICE 1
#define QUICKSORT_CHOICE 2
#define RADIXSORT_CHOICE 3

int* CreateRandomArray(int array_type, int size, int limit) {
	switch (array_type) {
		case UNIFORMAL_ARRAY:
			return CreateUniformalArray(size, limit);			
		case REPEATING_VALUE_ARRAY:
			return CreateRepeatingValueArray(size, limit, NON_UNIFORM_PORTION);
		default:
			return NULL;
	}
}

int* ReadArrayFromInput(int size) {
	int* arr = (int*)malloc(sizeof(int)*size);
	int i;

	printf("Enter array (in one line, sepereated by spaces):");
	for (i = 0; i < size; i++) {
		scanf("%d", arr + i);
	}

	return arr;
}

int* ReadArray(int* sizeRes, int* limitRes) {
	int size, choice, limit=0, i;
	int* arr = NULL;

	printf("Insert array size:\n");
	scanf("%d", &size);

	printf("Create array...\n");
	printf("1. From input\n");
	printf("2. Randomly (Uniformal distribution)\n");
	printf("3. Random with Repeating Value\n");
	scanf("%d", &choice);

	if (choice == 1) {
		arr = ReadArrayFromInput(size);
		for (i = 0; i < size; i++) {
			limit = max(limit, arr[i]);
		}
	}
	else {
		printf("Insert max value:\n");
		scanf("%d", &limit);
		arr = CreateRandomArray(NORMALIZE_ARRAY_TYPE(choice), size, limit);
	}

	*sizeRes = size;
	*limitRes = limit;
	return arr;
}

BOOL IsHeapsortAlgorithm(int algorithm) {
	return Contains(heapsort_algorithms, SAS(heapsort_algorithms), algorithm);
}

BOOL IsQuicksortAlgorithm(int algorithm) {
	return Contains(quicksort_algorithms, SAS(quicksort_algorithms), algorithm);
}

BOOL IsRadixsortAlgorithm(int algorithm) {
	return Contains(radixsort_algorithms, SAS(radixsort_algorithms), algorithm);
}

BOOL RunAlgorithm(int* arr, int size, int choice, int limit, int arity, int base, double* runtime) {
	BOOL ascending = TRUE;
	clock_t end;
	clock_t start = clock();
	switch (choice) {
		case ASCENDING_HEAPSORT:
			AscendingHeapSort(arr, size, arity);
			break;
		case DESCENDING_HEAPSORT:
			ascending = FALSE;
			DescendingHeapSort(arr, size, arity);
			break;
		case STACK_QUICKSORT:
			StackBasedQuickSort(arr, size);
			break;
		case QUEUE_QUICKSORT:
			QueueBasedQuickSort(arr, size);
			break;
		case REPEATING_VALUE_QUICKSORT:
			EqualElementQuickSort(arr, size, 0, size - 1);
			break;
		case RADIXSORT:
			BaseModuluRadixSort(arr, size, limit, base);
			break;
		default:
			break;
	}
	end = clock();
	*runtime = (double)(end - start) / CLOCKS_PER_SEC;
	return ValidateSortedArray(arr, size, ascending);
}

BOOL RunAlgorithmFromInput() {
	int* arr;
	int size, choice, limit, arity = 0, base = 0;
	double runtime;
	BOOL isSorted;

	arr = ReadArray(&size, &limit);
	printf("Array created!\n");
	PrintIntArray(arr, size);
	printf("\n");

	printf("Choose Algorithm:\n");
	printf("1. Ascending Heapsort\n");
	printf("2. Descending Heapsort\n");
	printf("3. Stack-Based Quicksort\n");
	printf("4. Queue-Based Quicksort\n");
	printf("5. Equal-Element Quicksort\n");
	printf("6. Base Modulu Radixsort\n");
	scanf("%d", &choice);
	printf("\n");

	if (IsHeapsortAlgorithm(choice)) {
		printf("Choose Arity:\n");
		scanf("%d", &arity);
		printf("\n");
	}
	else if (IsRadixsortAlgorithm(choice)) {
		printf("Choose Base:\n");
		scanf("%d", &base);
		printf("\n");
	}

	isSorted = RunAlgorithm(arr, size, choice, limit, arity, base, &runtime);
	if (isSorted) {
		printf("The Array Is Sorted!:\n");
	} else {
		printf("The Array Is NOT Sorted!:\n");
	}
	PrintIntArray(arr, size);
	printf("\n");
	printf("Algorithm Runtime: %f\n", runtime);

	free(arr);

	return isSorted;
}

BenchmarkConfig* CreateBenchmarkConfig(
	const int sizes[], int length, int limit, int arity, const int algorithms[], 
	int num_algos, int array_type, int iterations, int base) {
	int i;
	BenchmarkConfig *config = (BenchmarkConfig*)malloc(sizeof(BenchmarkConfig));
	config->sizes = sizes;
	config->length = length;
	config->limit = limit;
	config->arity = arity;
	config->algorithms = algorithms;
	config->num_algos = num_algos;
	config->array_type = array_type;
	config->iterations = iterations;
	config->base = base;
	config->runtimes = (double**)malloc(sizeof(double*)*num_algos);
	config->expected_runtimes = (double**)malloc(sizeof(double*)*num_algos);
	config->successes = (int**)malloc(sizeof(int*)*num_algos);
	for (i = 0; i < num_algos; i++) {
		config->runtimes[i] = (double*)calloc(sizeof(double), length);
		config->expected_runtimes[i] = (double*)calloc(sizeof(double), length);
		config->successes[i] = (int*)calloc(sizeof(int), length);
	}
	return config;
}

BenchmarkConfig* CreateHeapsortBenchmarkConfig(int arity) {
	if (arity != 1) {
		return CreateBenchmarkConfig(
			benchmark_heapsort, SAS(benchmark_heapsort), DEFAULT_LIMIT, arity, heapsort_algorithms,
			SAS(heapsort_algorithms), UNIFORMAL_ARRAY, NUM_ITERATIONS, 0);
	}
	else {
		return CreateBenchmarkConfig(
			benchmark_heapsort_arity_1, SAS(benchmark_heapsort_arity_1), DEFAULT_LIMIT, arity,
			heapsort_algorithms, SAS(heapsort_algorithms), UNIFORMAL_ARRAY, NUM_ITERATIONS, 0);
	}
}

BenchmarkConfig* CreateQuicksortBenchmarkConfig(int array_type) {
	return CreateBenchmarkConfig(
		benchmark_quicksort, SAS(benchmark_quicksort), DEFAULT_LIMIT, 0, quicksort_algorithms,
		SAS(quicksort_algorithms), array_type, NUM_ITERATIONS, 0);
}

BenchmarkConfig* CreateRadixsortBenchmarkConfig(int base) {
	return CreateBenchmarkConfig(
		benchmark_radixsort, SAS(benchmark_radixsort), RADIXSORT_LIMIT, 0, radixsort_algorithms,
		SAS(radixsort_algorithms), UNIFORMAL_ARRAY, NUM_ITERATIONS, base);
}

void FreeBenchmarkConfig(BenchmarkConfig* config) {
	free(config->runtimes);
	free(config->successes);
	free(config->expected_runtimes);
	free(config);
}

SingleConfig* CreateSingleConfig(int size, int limit, int arity, int algorithm, int array_type, int iterations) {
	SingleConfig *config = (SingleConfig*)malloc(sizeof(SingleConfig));
	config->size = size;
	config->limit = limit;
	config->arity = arity;
	config->algorithm = algorithm;
	config->array_type = array_type;
	config->runtime = 0;
	config->successes = 0;
	config->iterations = iterations;
	return config;
}

void FreeSingleConfig(SingleConfig* config) {
	free(config);
}

BOOL HasAlgorithm(BenchmarkConfig* config, BOOL (*predicate)(int)) {
	int i;
	for (i = 0; i < config->num_algos; i++) {
		if ((*predicate)(config->algorithms[i])) {
			return TRUE;
		}
	}
	return FALSE;
}

BOOL HasHeapsortAlgorithm(BenchmarkConfig* config) {
	return HasAlgorithm(config, &IsHeapsortAlgorithm);
}

BOOL HasQuicksortAlgorithm(BenchmarkConfig* config) {
	return HasAlgorithm(config, &IsQuicksortAlgorithm);
}

BOOL HasRadixsortAlgorithm(BenchmarkConfig* config) {
	return HasAlgorithm(config, &IsRadixsortAlgorithm);
}

double ExpectedHeapsortRuntime(int size, int arity) {
	if (arity == 1) {
		return size*size;
	}
	return size*arity*log(size) / log(arity);	
}

double ExpectedQuicksortRuntime(int size, int array_type, int algorithm) {
	if (array_type == REPEATING_VALUE_ARRAY &&
		Contains(iter_quicksort_algorithms, SAS(iter_quicksort_algorithms), algorithm)) {
		return size*size;
	}
	return size*log(size) / log(2);
}

double ExpectedRadixsortRuntime(int size, int limit, int base) {
	return (base + size)*log(limit) / log(base);
}

double ExpectedAlgorithmRuntime(int size, int limit, int arity, int array_type, int algorithm) {
	if (IsHeapsortAlgorithm(algorithm)) {
		return ExpectedHeapsortRuntime(size, arity);
	}
	if (IsQuicksortAlgorithm(algorithm)) {
		return ExpectedQuicksortRuntime(size, array_type, algorithm);
	}
	if (IsRadixsortAlgorithm(algorithm)) {
		return ExpectedRadixsortRuntime(size, limit, size); // UPDATE!!!
	}
	return 0.0;
}

void RunBenchmarkConfig(BenchmarkConfig* config) {
	int size_index, size, algo_index, algorithm, iteration, *arr;
	double runtime;
	BOOL succeeded;

	for (size_index = 0; size_index < config->length; size_index++) {
		size = config->sizes[size_index];
		for (iteration = 0; iteration < config->iterations; iteration++) {
			arr = CreateRandomArray(config->array_type, size, config->limit);
			for (algo_index = 0; algo_index < config->num_algos; algo_index++) {
				algorithm = config->algorithms[algo_index];
				succeeded =
					RunAlgorithm(arr, size, algorithm, config->limit, config->arity, config->base, &runtime);
				if (succeeded) {
					config->successes[algo_index][size_index]++;
					config->runtimes[algo_index][size_index] += runtime;
				}
				config->runtimes[algo_index][size_index] /= config->successes[algo_index][size_index];
				config->expected_runtimes[algo_index][size_index] =
					ExpectedAlgorithmRuntime(size, config->limit, config->arity, config->array_type, algorithm);				
			}
			free(arr);
		}
	}
}

BOOL IsOmegaOf(double* runtimes, double* expected_runtimes, int length) {
	int i;
	double* differences = (double*)malloc(sizeof(double)*length - 1);
	int balance = 0;	
	for (i = 0; i < length-1; i++) {
		//differences[i] = (runtimes[i+1] / runtimes[i]) - (expected_runtimes[i+1] / expected_runtimes[i]);		
		differences[i] = (RUNTIME_MULTIPLIER*runtimes[i+1] / expected_runtimes[i + 1]) -
						 (RUNTIME_MULTIPLIER*runtimes[i] / expected_runtimes[i]);
		printf("difference %d: %10f\n", i, differences[i]);
	}
	for (i = 0; i < length - 2; i++) {		
		balance += ((differences[i + 1] < 0) || (differences[i + 1] < differences[i])) ? -i : i;		
	}
	return balance < 0;
}

char* GetAlgorithmName(int algorithm) {
	switch (algorithm) {
		case ASCENDING_HEAPSORT:
			return "Asecnding Heapsort";
		case DESCENDING_HEAPSORT:
			return "Descending Heapsort";
		case STACK_QUICKSORT:
			return "Stack Quicksort";
		case QUEUE_QUICKSORT:	
			return "Queue Quicksort";	
		case REPEATING_VALUE_QUICKSORT:
			return "Repeating Value Quicksort";
		case RADIXSORT:
			return "Radixsort";
		default:
			return "";
	}
}

char* GetArrayTypeName(int array_type) {
	switch (array_type) {
		case UNIFORMAL_ARRAY:
			return "Uniformal Array";
		case REPEATING_VALUE_ARRAY:
			return "Repeating Value Array";
		default:
			return "";
	}
}

void PrintGeneralConfigInfo(BenchmarkConfig* config) {
	printf("Benchmark Report:\n");
	printf("================\n");
	printf("iterations: %d\n", config->iterations);
	printf("limit: %d\n", config->limit);
	if (HasHeapsortAlgorithm(config)) {
		printf("arity: %d\n", config->arity);
	}
	if (HasQuicksortAlgorithm(config)) {
		printf("array type: %s\n", GetArrayTypeName(config->array_type));
	}
}

void PrintBenchmarkResults(BenchmarkConfig* config) {
	static const char* spaces_pad = "                                                  "; // length = 50
	static const char* runtime_pad = "";
	static const char* sizes_str = "Sizes:";
	static const int algo_name_length = 30;
	static const int sizes_length = 15;
	static const int percentage_precision = 2;
	static const int runtime_precision = 12;

	int size_index, algo_index;
	char* algo_name;

	PrintGeneralConfigInfo(config);

	printf("-------------------------------\n");
	printf("Successful sorting percentages:\n");
	printf("-------------------------------\n");
	printf("%s%.*s", sizes_str, algo_name_length - strlen(sizes_str), spaces_pad);
	for (size_index = 0; size_index < config->length; size_index++) {
		printf("%*d", sizes_length, config->sizes[size_index]);
	}
	printf("\n");
	for (algo_index = 0; algo_index < config->num_algos; algo_index++) {		
		algo_name = GetAlgorithmName(config->algorithms[algo_index]);
		printf("%s%.*s", algo_name, algo_name_length - strlen(algo_name), spaces_pad);
		for (size_index = 0; size_index < config->length; size_index++) {			
			printf("%*.*f", sizes_length, percentage_precision,
				   ((double)100*config->successes[algo_index][size_index])/config->iterations);
		}
		printf("\n");
	}

	printf("------------------------------------\n");
	printf("Successful sorting runtime averages:\n");
	printf("------------------------------------\n");
	printf("%s%.*s", sizes_str, algo_name_length - strlen(sizes_str), spaces_pad);
	for (size_index = 0; size_index < config->length; size_index++) {
		printf("%*d", sizes_length, config->sizes[size_index]);
	}
	printf("\n");
	for (algo_index = 0; algo_index < config->num_algos; algo_index++) {
		algo_name = GetAlgorithmName(config->algorithms[algo_index]);
		printf("%s%.*s", algo_name, algo_name_length - strlen(algo_name), spaces_pad);
		for (size_index = 0; size_index < config->length; size_index++) {			
			printf("%*.*f", sizes_length, runtime_precision, config->runtimes[algo_index][size_index]);
		}
		printf("\n");
	}

	//printf("-------------------------------------------------\n");
	//printf("Does runtime seem to diverge to expected runtime:\n");
	//printf("-------------------------------------------------\n");
	//for (algo_index = 0; algo_index < config->num_algos; algo_index++) {
	//	algo_name = GetAlgorithmName(config->algorithms[algo_index]);
	//	printf("%s%.*s", algo_name, algo_name_length - strlen(algo_name), spaces_pad);
	//	if (IsOmegaOf(config->runtimes[algo_index],
	//				  config->expected_runtimes[algo_index], config->length)) {
	//		printf("YES!\n");
	//	}
	//	else {
	//		printf("NO!\n");
	//	}
	//}
	printf("--------------------------\n");
	printf("Benchmark report complete.\n");
	printf("--------------------------\n");
}

void RunBenchmarkTest() {
	BenchmarkConfig* test;
	int choice, arity = 0, base = 0, array_type = 0;
	printf("Choose benchmark test:\n");
	printf("1. Heapsort Algorithms:\n");
	printf("2. Quicksort Algorithms:\n");
	printf("3. Radixsort Algorithm:\n");
	scanf("%d", &choice);
	printf("\n");

	switch (choice) {
		case HEAPSORT_CHOICE:
			printf("Choose Arity:\n");
			scanf("%d", &arity);		
			test = CreateHeapsortBenchmarkConfig(arity);
			break;
		case QUICKSORT_CHOICE:
			printf("Choose Array Type:\n");
			printf("1. Uniformal Array:\n");
			printf("2. Reapting Value Array:\n");
			printf("Choose Array Type:\n");
			scanf("%d", &array_type);
			test = CreateQuicksortBenchmarkConfig(array_type);
			break;
		case RADIXSORT_CHOICE:
			printf("Choose Base:\n");
			scanf("%d", &base);		
			test = CreateRadixsortBenchmarkConfig(base);
			break;
		default:
			break;
	}

	printf("\n");
	RunBenchmarkConfig(test);
	PrintBenchmarkResults(test);
	FreeBenchmarkConfig(test);
}

void main() {	
	int choice = QUIT;
	BOOL result;
	do {
		printf("Choose run-mode:\n");
		printf("1. Input test manually\n");
		printf("2. Run built-in benchmark\n");
		scanf("%d", &choice);
		printf("\n");

		switch (choice) {
		case MANUAL_MODE:
			result = RunAlgorithmFromInput();
			break;
		case BUILTIN_MODE:
			RunBenchmarkTest();
			break;
		default:
			break;
		}
				
		printf("\nGo again?\n");
		printf("1. Yes\n");
		printf("2. No\n");
		scanf("%d", &choice);
		printf("\n");
	} while (choice == CONTINUE);

}