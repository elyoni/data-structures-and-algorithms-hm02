#pragma once

// Sorts the given 'arr' array of size 'size' in an ascending order using a MinPriorityQueue
// with arity 'arity'.
// RUNTIME: O(n*d*log_d_n), where d = arity. If d == 1, the runtime is O(n^2).
void AscendingHeapSort(int* arr, int size, int arity);
// Sorts the given 'arr' array of size 'size' in a descending order using a MinPriorityQueue
// with arity 'arity'.
// RUNTIME: O(n*d*log_d_n), where d = arity. If d == 1, the runtime is O(n^2).
void DescendingHeapSort(int* arr, int size, int arity);

// Sorts the given 'arr' array of size 'size' using Quicksort in a non-recursive manner - 
// using a single stack which is limited to holding (2 * cieling(log(n))) elements.
// Uses the regular Partition algorithm for partitioning the current sub-array.
// RUNTIME: O(nlogn) on average, would be approaching O(n^2) when arrays have many repeating values.
// SPACE COMPLEXITY: O(logn) (The algorithm never requires mores than O(logn) memory).
void StackBasedQuickSort(int* arr, int size);
// Sorts the given 'arr' array of size 'size' using Quicksort in a non-recursive manner - 
// using a single queue which is limited to holding (2 * cieling(log(n))) elements.
// Uses the regular Partition algorithm for partitioning the current sub-array.
// RUNTIME: O(nlogn) on average, would be approaching O(n^2) when arrays have many repeating values.
// SPACE COMPLEXITY: O(n) (The algorithm never requires mores than O(n) memory).
void QueueBasedQuickSort(int* arr, int size);
// Sorts the given 'arr' array of size 'size' using Quicksort recursive manner.
// Uses the Smart Partition algorithm for partitioning the current sub-array - the smart
// partition algorithm divides the array into 4 (instead of 3) sub-arrays:
// [(elements smaller than pivot)(elements equal to pivot)(pivot)(elements larger than pivot)].
// RUNTIME: O(nlogn) on average, repeating values actually shorten algorithm runtime in practice.
void EqualElementQuickSort(int* arr, int size, int p, int r);

// Sorts the given 'arr' array of size 'size' using Modulu Radixsort - in which the given 'base'
// is used as the base for comparison in each iteration. Radixsort uses Counting Sort with values
// ranging between [0, base-1] inclusive in each iteration. Think how.
// RUNTIME: O((n+k)*log_k_n), where n=limit and k=base.
void BaseModuluRadixSort(int* arr, int size, int limit, int base);

